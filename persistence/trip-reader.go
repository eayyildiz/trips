package persistence

import (
	"bitaksi/models"
	"context"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//TripReader is used for query Trip collection
type TripReader interface {
	GetAllTrips() ([]models.Trip, error)
	GetTrips(longitude float64, latitude float64, radius int, startDate *time.Time, endDate *time.Time) ([]models.Trip, error)
	GetTripVehicles(longitude float64, latitude float64, radius int) (map[string]int32, error)
	GetMinDistanceTrip(longitude float64, latitude float64, radius int) (*models.Trip, error)
	GetMaxDistanceTrip(longitude float64, latitude float64, radius int) (*models.Trip, error)
}

type tripReader struct {
	ctx        context.Context
	collection *mongo.Collection
}

// NewTripReader creates new instance of TripReader
func NewTripReader(database *mongo.Database) TripReader {
	return &tripReader{
		ctx:        context.Background(),
		collection: database.Collection("trips"),
	}
}

func (tr *tripReader) GetAllTrips() ([]models.Trip, error) {
	var trips []models.Trip
	cursor, err := tr.collection.Find(tr.ctx, bson.M{})

	if err != nil {
		return nil, err
	}

	if err = cursor.All(tr.ctx, &trips); err != nil {
		return nil, err
	}

	return trips, nil
}

func (tr *tripReader) GetTrips(longitude float64, latitude float64, radius int, startDate *time.Time, endDate *time.Time) ([]models.Trip, error) {
	var trips []models.Trip
	filter := bson.M{
		"start": bson.M{
			"$nearSphere": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{longitude, latitude},
				},
				"$maxDistance": radius * 1000,
			},
		},
	}

	if startDate != nil {
		filter["start_date"] = bson.M{"$gte": startDate}
	}

	if endDate != nil {
		filter["complete_date"] = bson.M{"$lte": endDate}
	}

	cursor, err := tr.collection.Find(tr.ctx, filter)
	if err != nil {
		return nil, err
	}

	if err = cursor.All(tr.ctx, &trips); err != nil {
		return nil, err
	}

	return trips, nil
}

func (tr *tripReader) GetTripVehicles(longitude float64, latitude float64, radius int) (map[string]int32, error) {
	var result = make(map[string]int32)

	matchStage := bson.D{{"$geoNear",
		bson.M{"near": bson.M{"type": "Point", "coordinates": []float64{longitude, latitude}},
			"distanceField": "distancefield",
			"key":           "start",
			"maxDistance":   radius * 1000,
		},
	}}

	groupStage := bson.D{{"$group", bson.D{{"_id", "$year"}, {"count", bson.D{{"$sum", 1}}}}}}

	var mongoResponse []bson.M

	cursor, err := tr.collection.Aggregate(tr.ctx, mongo.Pipeline{matchStage, groupStage})
	if err != nil {
		return nil, err
	}

	if err = cursor.All(tr.ctx, &mongoResponse); err != nil {
		return nil, err
	}

	for i := 0; i < len(mongoResponse); i++ {
		key := strconv.Itoa(int(mongoResponse[i]["_id"].(int32)))
		value := mongoResponse[i]["count"].(int32)

		result[key] = value
	}

	return result, nil
}

func (tr *tripReader) GetMaxDistanceTrip(longitude float64, latitude float64, radius int) (*models.Trip, error) {
	var trip models.Trip
	filter := bson.M{
		"start": bson.M{
			"$nearSphere": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{longitude, latitude},
				},
				"$maxDistance": radius * 1000,
			},
		},
	}

	findOptions := options.FindOneOptions{}
	findOptions.SetSort(bson.D{{"distance_travelled", -1}})

	singleResult := tr.collection.FindOne(tr.ctx, filter, &findOptions)

	if err := singleResult.Decode(&trip); err != nil {
		return nil, err
	}

	return &trip, nil
}

func (tr *tripReader) GetMinDistanceTrip(longitude float64, latitude float64, radius int) (*models.Trip, error) {
	var trip models.Trip
	filter := bson.M{
		"start": bson.M{
			"$nearSphere": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{longitude, latitude},
				},
				"$maxDistance": radius * 1000,
			},
		},
	}

	findOptions := options.FindOneOptions{}
	findOptions.SetSort(bson.D{{"distance_travelled", 1}})

	singleResult := tr.collection.FindOne(tr.ctx, filter, &findOptions)

	if err := singleResult.Decode(&trip); err != nil {
		return nil, err
	}

	return &trip, nil
}
