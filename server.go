package main

import "bitaksi/cmd"

// @title BiTaksi Case Api
// @version 1.0
// @description This is a sample BiTaksi case server.

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

// @host bitaksi-case.herokuapp.com
// @BasePath /
func main() {
	cmd.Execute()
}
