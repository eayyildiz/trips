package requests

import "time"

type GetTripRequest struct {
	Longitude float64   `json:"longitude" form:"longitude" query:"longitude"`
	Latitude  float64   `json:"latitude" form:"latitude" query:"latitude"`
	Radius    int       `json:"radius" form:"radius" query:"radius"`
	StartDate *DateTime `json:"start_date" form:"start_date" query:"start_date"`
	EndDate   *DateTime `json:"end_date" form:"end_date" query:"end_date"`
}

type DateTime time.Time

func (d *DateTime) UnmarshalParam(src string) error {
	ts, err := time.Parse(time.RFC3339, src)
	*d = DateTime(ts)

	return err
}
