package controllers

import (
	"bitaksi/controllers/requests"
	"bitaksi/persistence"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

type TripController interface {
	GetTrips(c echo.Context) error
	GetTripVehicles(c echo.Context) error
	GetMinMaxDistances(c echo.Context) error
}

type tripController struct {
	tripReader persistence.TripReader
}

func NewTripController(reader persistence.TripReader) TripController {
	return &tripController{
		tripReader: reader,
	}
}

// @Tags Trips
// @Summary Get Trips
// @Produce  json
// @Param longitude query float64 true "Longitude"
// @Param latitude query float64 true "Latitude"
// @Param radius query int true "Radius (in Kilometers)"
// @Param start_date query time.Time false "StartDate"
// @Param end_date query time.Time false "EndDate"
// @Success 200 {object} []models.Trip "response"
// @Failure 401 {string} string "error"
// @Failure 400 {string} string "error"
// @Security ApiKeyAuth
// @Router /trips [get]
func (tc *tripController) GetTrips(c echo.Context) error {
	request := new(requests.GetTripRequest)
	if err := c.Bind(request); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return nil
	}

	trips, err := tc.tripReader.GetTrips(request.Longitude, request.Latitude, request.Radius, (*time.Time)(request.StartDate), (*time.Time)(request.EndDate))
	if err != nil {
		c.NoContent(204)
		return err
	}

	c.JSON(http.StatusOK, trips)
	return nil
}

// @Tags Trips
// @Summary Get Vehicles trip count
// @Produce  json
// @Param longitude query float64 true "Longitude"
// @Param latitude query float64 true "Latitude"
// @Param radius query int true "Radius (in Kilometers)"
// @Success 200 {string} string "response"
// @Failure 401 {string} string "error"
// @Failure 400 {string} string "error"
// @Security ApiKeyAuth
// @Router /trips/vehicles [get]
func (tc *tripController) GetTripVehicles(c echo.Context) error {
	request := new(requests.GetTripRequest)
	if err := c.Bind(request); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return nil
	}

	result, err := tc.tripReader.GetTripVehicles(request.Longitude, request.Latitude, request.Radius)
	if err != nil {
		c.JSON(400, fmt.Sprint(err))
		return nil
	}

	c.JSON(http.StatusOK, result)

	return nil
}

// @Tags Trips
// @Summary Get MinMax distanced trips
// @Produce  json
// @Param longitude query float64 true "Longitude"
// @Param latitude query float64 true "Latitude"
// @Param radius query int true "Radius (in Kilometers)"
// @Success 200 {object} map[string]models.Trip "response"
// @Failure 401 {string} string "error"
// @Failure 400 {string} string "error"
// @Security ApiKeyAuth
// @Router /trips/distances [get]
func (tc *tripController) GetMinMaxDistances(c echo.Context) error {
	response := make(map[string]interface{})

	request := new(requests.GetTripRequest)
	if err := c.Bind(request); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return nil
	}

	maxDistancedTrip, err := tc.tripReader.GetMaxDistanceTrip(request.Longitude, request.Latitude, request.Radius)
	if err != nil {
		c.JSON(http.StatusInternalServerError, fmt.Sprint(err))
		return nil
	}

	minDistancedTrip, err := tc.tripReader.GetMinDistanceTrip(request.Longitude, request.Latitude, request.Radius)
	if err != nil {
		c.JSON(http.StatusInternalServerError, fmt.Sprint(err))
		return nil
	}

	response["max"] = maxDistancedTrip
	response["min"] = minDistancedTrip

	c.JSON(http.StatusOK, response)

	return nil
}
