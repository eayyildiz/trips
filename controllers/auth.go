package controllers

import (
	"bitaksi/controllers/requests"
	"bitaksi/models"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

type AuthController interface {
	Login(c echo.Context) error
}

type authController struct {
	Users map[string]models.UserModel
}

func NewAuthController() AuthController {
	return &authController{
		Users: make(map[string]models.UserModel),
	}
}

// @Tags Auth
// @Summary Auth user
// @Description Login as user
// @Produce  json
// @Param username formData string true "Username"
// @Param password formData string true "Password"
// @Success 200 {object} string
// @Failure 401 {object} string
// @Router /login [post]
func (ac *authController) Login(c echo.Context) error {
	request := new(requests.LoginRequest)
	if err := c.Bind(request); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return echo.ErrBadRequest
	}

	// Throws unauthorized error
	if request.Username != "bitaksi" || request.Password != "bipass" {
		return echo.ErrUnauthorized
	}

	// Create new token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = "Bitaksi admin"
	claims["admin"] = true
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}
