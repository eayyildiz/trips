package models

// UserModel model represents single user data
type UserModel struct {
	Username string
	Password string
}
