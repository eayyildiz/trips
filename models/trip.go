package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Trip struct {
	ID                   primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	DistanceTravelled    int32              `bson:"distance_travelled,omitempty,omitempty" json:"distance_travelled,omitempty"`
	DriverRating         float64            `bson:"driver_rating,omitempty" json:"driver_rating"`
	RiderRating          float64            `bson:"rider_rating,omitempty" json:"rider_rating"`
	StartZipCode         int32              `bson:"start_zip_code,omitempty" json:"start_zip_code"`
	EndZipCode           string             `bson:"end_zip_code,omitempty" json:"end_zip_code"`
	CharityID            int32              `bson:"charity_id,omitempty" json:"charity_id"`
	RequestedCarCategory string             `bson:"requested_car_category,omitempty" json:"requested_car_category"`
	FreeCreditUsed       float64            `bson:"free_credit_used,omitempty" json:"free_credit_used"`
	SurgeFactor          int32              `bson:"surge_factor,omitempty" json:"surge_factor"`
	Color                string             `bson:"color,omitempty" json:"color"`
	Make                 string             `bson:"make,omitempty" json:"make"`
	Model                string             `bson:"model,omitempty" json:"model"`
	Year                 int32              `bson:"year,omitempty" json:"year"`
	Rating               float64            `bson:"rating,omitempty" json:"rating"`
	Date                 string             `bson:"Date,omitempty" json:"Date"`
	PRCP                 float64            `bson:"PRCP,omitempty" json:"PRCP"`
	TMAX                 int32              `bson:"TMAX,omitempty" json:"TMAX"`
	TMIN                 int32              `bson:"TMIN,omitempty" json:"TMIN"`
	AWND                 float64            `bson:"AWND,omitempty" json:"AWND"`
	GustSpeed2           float64            `bson:"GustSpeed2,omitempty" json:"gust_speed2"`
	Fog                  int32              `bson:"Fog,omitempty" json:"Fog"`
	Heavy                int32              `bson:"HeavyFog,omitempty" json:"heavy_fog"`
	Thunder              int32              `bson:"Thunder,omitempty" json:"thunder_fog"`
	Start                GeoJson            `bson:"start,omitempty" json:"start"`
	End                  GeoJson            `bson:"end,omitempty" json:"end"`
	StartDate            time.Time          `bson:"start_date,omitempty" json:"start_date"`
	CompleteDate         time.Time          `bson:"complete_date,omitempty" json:"complete_date"`
}

type GeoJson struct {
	Type        string    `json:"-"`
	Coordinates []float64 `json:"coordinates"`
}
