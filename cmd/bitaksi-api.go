package cmd

import (
	"bitaksi/controllers"
	"net/http"

	"bitaksi/persistence"
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	echoSwagger "github.com/swaggo/echo-swagger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"

	_ "bitaksi/docs"
)

var config string
var rootCmd = &cobra.Command{
	Use:   "bitaksi-webapi",
	Short: "BiTaksi Web Api",
	Long:  "Backend Coding Challenge",
	PersistentPreRun: func(ccmd *cobra.Command, args []string) {
		if config != "" {
			abs, _ := filepath.Abs(config)
			base := filepath.Base(abs)
			path := filepath.Dir(abs)

			viper.SetConfigName(strings.Split(base, ".")[0])
			viper.AddConfigPath(path)
			viper.AutomaticEnv()

			if err := viper.ReadInConfig(); err != nil {
				os.Exit(1)
			}
		} else {
			fmt.Println("Please provide config file like, `--config config.yml`")
			os.Exit(1)
		}
	},
	Run: runApplication,
}

func init() {
	rootCmd.Flags().StringVar(&config, "config", "", "/path/to/config.yml")
}

func runApplication(cmd *cobra.Command, args []string) {
	environment := viper.Get("application.env").(string)
	//port := viper.Get("application.port").(string)
	port := viper.Get("PORT").(string)

	mongodbConnStr := viper.Get("mongodb.connectionstr").(string)

	// Create logger
	var logger *zap.Logger

	if environment == "prod" {
		logger, _ = zap.NewProduction()
	} else {
		logger, _ = zap.NewDevelopment()
	}

	defer logger.Sync()

	// Mongo Collection
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongodbConnStr))
	if err != nil {
		panic(err)
	}
	defer client.Disconnect(ctx)

	database := client.Database("case")

	// Persistence Readers
	tripReader := persistence.NewTripReader(database)

	// Controllers
	authController := controllers.NewAuthController()
	tripController := controllers.NewTripController(tripReader)

	// Echo Server
	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.GET("", Home)

	// Swagger
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	// Auth endpoints
	e.POST("/login", authController.Login)

	// Trip endpoints
	tripsGroup := e.Group("/trips")
	tripsGroup.Use(middleware.JWT([]byte("secret")))
	tripsGroup.GET("", tripController.GetTrips)
	tripsGroup.GET("/vehicles", tripController.GetTripVehicles)
	tripsGroup.GET("/distances", tripController.GetMinMaxDistances)

	e.Logger.Fatal(e.Start(":" + port))
}

// Execute is a trigger of root commands execute method
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func Home(c echo.Context) error {
	c.String(http.StatusOK, `
	          [\
	     .----' '-----.
            //^^^^;;^^^^^^'\
    _______//_____||_____()_\________
   /826    :      : ___              '\
  |>   ____;      ;  |/\><|   ____   _<)
 {____/    \_________________/    \____}
      \ '' /                 \ '' /
jgs    '--'                   '--'
`)
	return nil
}
