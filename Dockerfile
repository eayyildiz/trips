FROM golang:latest

WORKDIR /go/src/bitaksi
COPY . .

RUN go mod verify
RUN go build

EXPOSE $PORT

CMD ["./bitaksi", "--config", "./configs/default.yml" ]